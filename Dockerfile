FROM node:6.11.2-alpine
LABEL "maintainer": "adam.berkowitz@uconn.edu" \
      "name": "Adam Berkowitz" \
      "project": "node test"

WORKDIR /project

COPY ./package.json /project/package.json
COPY ./local-dev-entrypoint.sh /entrypoint.sh

RUN apk update && apk add bash \
    && chmod +x /entrypoint.sh \
    && npm set progress=false \
    && npm install -g yarn gulp-cli grunt-cli webpack \
    && yarn add --dev --no-progress gulp grunt webpack \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 3000

ENTRYPOINT ["/entrypoint.sh"]