# Local development image

## Options
Default: This will run the default task of the gulpfile

These other options will override main task with gulp watch
- `entrypoint: ["gulp", "watch"]`
- `entrypoint: ["bash", "/project/entrypoint.sh]` (if it's mounted correctly)
- `entrypoint: ["npm", "run", "{task}"]`

Testing needs to be done for other task runners/asset managers
- grunt
- webpack